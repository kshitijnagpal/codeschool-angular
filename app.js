(function (){
    //module
    var app = angular.module('store', []);

    //controller
    app.controller('StoreController', function(){
        this.products = gems;
    });

    //gem object
    var gems=[
        {
            name: 'Awesome gem',
            price: 2.95,
            description: 'Freakin awesome gem!',
            canPurchase: true,
            soldOut: false,
        },
        {
            name: 'Awesomest gem',
            price: 5.95,
            description: 'Freakin awesomest gem!',
            canPurchase: false,
        }
    ]


})();
